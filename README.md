# semantic-release-test2

Testing automated semantic release project

This project uses
[semantic-release-gitlab](https://gitlab.com/hutson/semantic-release-gitlab)
to automatically publish new NPM versions based on commit messages.

To fetch release notes from the command line use
[available-versions](https://github.com/bahmutov/available-versions#available-versions)

```sh
$ as-a gitlab vers semantic-release-test2
removed https://gitlab.com/bahmutov/semantic-release-test.git
semantic-release-test2 from git+ssh://git@gitlab.com/bahmutov/semantic-release-test.git
---------------------------------------------------------------------------------------
version  age        dist-tag  release
-------  ---------  --------  ------------------------------------------------------
1.0.0    2 minutes  latest    chore(name): changed name because it was taken already
```
